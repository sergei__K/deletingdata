package DeletingData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class DeletingData {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();
        ArrayList<Integer> integers = new ArrayList<>();
        Collections.addAll(integers, 1, 2, 34, 5, 6, 78, 9, 12);
        for (int i = 0; i < integers.size(); i++) {
            if (integers.get(i) == value) {
                integers.remove(integers.get(i));
            }
        }
        System.out.println(integers);
    }
}
